package com.fahmy.testproject.data.network;

import com.fahmy.testproject.data.network.model.ArticlesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiHelper {

    @GET("7.json")
    Call<ArticlesResponse> getArticlesFromApi(@Query("api-key") String api_KEy);
}
