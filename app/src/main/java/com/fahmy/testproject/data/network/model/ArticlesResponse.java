package com.fahmy.testproject.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ArticlesResponse {

    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("num_results")
    private int num_results;
    @Expose
    @SerializedName("results")
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("id")
        private Long id;
        @Expose
        @SerializedName("asset_id")
        private Long asset_id;
        @Expose
        @SerializedName("source")
        private String source;
        @Expose
        @SerializedName("published_date")
        private String published_date;
        @Expose
        @SerializedName("section")
        private String section;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("abstract")
        private String abstractSection;
        @Expose
        @SerializedName("byline")
        private String byline;
        @Expose
        @SerializedName("media")
        private List<Media> media;

        public static class Media {
            @Expose
            @SerializedName("media-metadata")
            private List<Metadata> media_metadata;

            public List<Metadata> getMedia_metadata() {
                return media_metadata;
            }

            public void setMedia_metadata(List<Metadata> media_metadata) {
                this.media_metadata = media_metadata;
            }

            public static class Metadata {
                @Expose
                @SerializedName("url")
                private String url;
                @Expose
                @SerializedName("format")
                private String format;
                @Expose
                @SerializedName("height")
                private int height;
                @Expose
                @SerializedName("width")
                private int width;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getFormat() {
                    return format;
                }

                public void setFormat(String format) {
                    this.format = format;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }
            }
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getAsset_id() {
            return asset_id;
        }

        public void setAsset_id(Long asset_id) {
            this.asset_id = asset_id;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getPublished_date() {
            return published_date;
        }

        public void setPublished_date(String published_date) {
            this.published_date = published_date;
        }

        public String getSection() {
            return section;
        }

        public void setSection(String section) {
            this.section = section;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAbstractSection() {
            return abstractSection;
        }

        public void setAbstractSection(String abstractSection) {
            this.abstractSection = abstractSection;
        }

        public List<Media> getMedia() {
            return media;
        }

        public void setMedia(List<Media> media) {
            this.media = media;
        }

        public String getByline() {
            return byline;
        }

        public void setByline(String byline) {
            this.byline = byline;
        }
    }
}
