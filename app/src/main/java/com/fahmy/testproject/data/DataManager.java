package com.fahmy.testproject.data;

import com.fahmy.testproject.data.network.ApiHelper;

public interface DataManager extends ApiHelper {

    ApiHelper getApiHelper();
}
