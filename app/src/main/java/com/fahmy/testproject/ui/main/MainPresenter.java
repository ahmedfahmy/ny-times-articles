package com.fahmy.testproject.ui.main;

import com.fahmy.testproject.data.DataManager;
import com.fahmy.testproject.data.network.model.ArticlesResponse;
import com.fahmy.testproject.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void getArticlesFromApi(int pageNo, Boolean isReloaded, String api_key) {
        if (getMvpView().inNetworkConnected()) {

            getMvpView().showLoading();

            getDataManager().getApiHelper().getArticlesFromApi(api_key)
                    .enqueue(new Callback<ArticlesResponse>() {
                        @Override
                        public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                            getMvpView().hideLoading();
                            if (response.code() == 200 && response.body() != null) {
                                getMvpView().renderArticlesListToUI(response.body());
                            } else {
                                getMvpView().retryLoadingArticlesList();
                            }
                        }

                        @Override
                        public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                            getMvpView().hideLoading();
                            getMvpView().retryLoadingArticlesList();
                        }
                    });
        } else {
            // NetworkNotConnected
            getMvpView().onErrorMessageReceived("Network Error");
        }
    }
}

