package com.fahmy.testproject.ui.main;

import com.fahmy.testproject.data.network.model.ArticlesResponse;
import com.fahmy.testproject.ui.base.ActivityMvpView;

public interface MainMvpView extends ActivityMvpView {

    void renderArticlesListToUI(ArticlesResponse articlesResponse);

    void retryLoadingArticlesList();

    void onErrorMessageReceived(String errorMessage);
}
