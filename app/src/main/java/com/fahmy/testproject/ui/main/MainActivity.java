package com.fahmy.testproject.ui.main;

import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fahmy.testproject.R;
import com.fahmy.testproject.data.network.model.ArticlesResponse;
import com.fahmy.testproject.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainMvpView {

    // MainActivity presenter
    @Inject
    MainMvpPresenter<MainMvpView> presenter;
    @BindView(R.id.refresh_view)
    SwipeRefreshLayout refreshView;
    @BindView(R.id.articles_list)
    RecyclerView articles_list;

    private Adapter articlesAdapter;
    private List<ArticlesResponse.Data> articlesLists = new ArrayList<ArticlesResponse.Data>();
    private int pageNo = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // initialize views
        init();

        handleViews();
    }

    @Override
    protected void init() {
        // initialize presenter injection
        getActivityComponent().inject(this);

        articlesAdapter = new ArticlesAdapter(articlesLists, this);

        articles_list.setHasFixedSize(true);
        articles_list.setLayoutManager(initLayoutManager());
        articles_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        articles_list.setAdapter(articlesAdapter);

        presenter.onAttach(this);
        presenter.getArticlesFromApi(pageNo, false, getString(R.string.api_key));
    }

    private LinearLayoutManager initLayoutManager() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(false);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        return mLayoutManager;
    }

    private void handleViews() {
        pageNo = 1;
        refreshView.setOnRefreshListener(() -> presenter.getArticlesFromApi(pageNo, false, getString(R.string.api_key)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    @Override
    public void renderArticlesListToUI(ArticlesResponse response) {
        refreshView.setRefreshing(false);
        if (response != null) {
            if (response.getData().size() >= 1) {
                articlesLists.addAll(response.getData());
            }
            articlesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void retryLoadingArticlesList() {
        refreshView.setRefreshing(false);
        refreshView.setOnRefreshListener(() -> presenter.getArticlesFromApi(pageNo, false, getString(R.string.api_key)));
    }

    @Override
    public void onErrorMessageReceived(String advice) {
        refreshView.setRefreshing(false);
        //render Error Message
    }
}
