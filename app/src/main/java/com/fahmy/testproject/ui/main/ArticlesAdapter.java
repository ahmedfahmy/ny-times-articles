package com.fahmy.testproject.ui.main;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.fahmy.testproject.R;
import com.fahmy.testproject.data.network.model.ArticlesResponse;
import java.util.List;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private List<ArticlesResponse.Data> articleList;
    private Context context;

    ArticlesAdapter(List<ArticlesResponse.Data> articleList, Context context) {
        // generate constructors to initialise the List and Context objects
        this.articleList = articleList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(context).inflate(R.layout.articles_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final ArticlesResponse.Data article = articleList.get(position);
        holder.articleTitle.setText(article.getTitle());
        holder.articleDate.setText(article.getPublished_date());
        holder.articleWriter.setText(article.getByline());

        Glide.with(context)
                .load(getUrl(article)).circleCrop()
                .error(R.drawable.ic_launcher_background)
                .into(holder.articleImage);
    }

    private String getUrl(ArticlesResponse.Data article) {
        String url = "";
        if (article.getMedia().size() > 0)
            url = article.getMedia().get(0).getMedia_metadata().get(0).getUrl();
        else
            url = "";
        return url;
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        TextView articleTitle;
        ImageView articleImage;
        TextView articleDate;
        TextView articleWriter;

        ViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects
            articleTitle = (TextView) itemView.findViewById(R.id.tv_article_title);
            articleImage = (ImageView) itemView.findViewById(R.id.iv_article_image);
            articleDate = (TextView) itemView.findViewById(R.id.tv_article_date);
            articleWriter = (TextView) itemView.findViewById(R.id.tv_article_writer);
        }

    }
}
