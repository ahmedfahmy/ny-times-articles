package com.fahmy.testproject.ui.main;

import com.fahmy.testproject.ui.base.ActivityMvpView;
import com.fahmy.testproject.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends ActivityMvpView> extends MvpPresenter<V> {

    void getArticlesFromApi(int pageNo, Boolean isReloaded, String api_key);
}
